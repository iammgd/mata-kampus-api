const express = require('express')
const port = process.env.PORT
require('./db/db')

const app = express()

app.use(express.json())

//router url
const router_user = require('./routers/user-router')
app.use(router_user)
const router_item = require('./routers/item-router')
app.use(router_item)



app.listen(port, () => {
    console.log(`Server running on port ${port}`)
})