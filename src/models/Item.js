let mongoose = require('mongoose');

// Item Schema

let itemSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    date: {
        type: String
    },
    time: {
        type: String
    },
    datetime: {
        type: Date,
        default: Date.now,
        required: true
    },
    description: {
        type: String
    },
    photo: {
        type: String,
        required: true
    },
    finder: {
        type: String,
        required: true
    }
});

let Item = module.exports = mongoose.model('Item', itemSchema);

module.exports = Item