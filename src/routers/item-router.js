const express = require('express')
const Item = require('../models/Item')
const auth = require('../middleware/auth')
const fs = require('fs');
const mime = require('mime');

const router = express.Router()


//index
router.get("/item/all/today", async(req, res) => {
    var dateFormat = require('dateformat');
    var day=dateFormat(new Date(), "yyyy/mm/dd");

	Item.find({date: day}).then(function (items) {
		res.send(items);
	});
})


//index
router.get("/item/:start_page/:per_page", auth, async(req, res) => {
	Item.find({finder: req.user._id}).skip(Number(req.params.start_page)).limit(Number(req.params.per_page)).then(function (items) {
		res.send(items);
	});
})

router.post('/item/photo/upload', auth, async (req, res, next) => {
    // to declare some path to store your converted image
    var matches = req.body.base64image.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
    response = {};
     
    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }
     
    response.type = matches[1];
    response.data = new Buffer.from(matches[2], 'base64');
    let decodedImg = response;
    let imageBuffer = decodedImg.data;
    let type = decodedImg.type;
    let extension = mime.getExtension(type);
    let fileName = "image_"+ req.user._id + "." + extension;
    try {
        fs.writeFileSync("./assets/uploads/images/item/" + fileName, imageBuffer, 'utf8');
        return res.send({"status":"success", "image_name": "item/"+fileName});
    } catch (e) {
        next(e);
    }
})

router.post('/item/create', auth, async (req, res) => {
    // Create a new user
    try {
        const body = req.body
        body.finder = req.user._id
        const item = new Item(body)
        await item.save()
        res.status(201).send({ item })
    } catch (error) {
        res.status(400).send(error)
    }
})

//detail
router.get("/item/:id", auth, async (req,res) => {
    //harus kasih where req.user._id
	Item.findById({_id : req.params.id})
	.then(data => res.send(data))
})

//edit
router.put("/item/update/:id", (req,res) => {
	Item.findOneAndUpdate({_id : req.params.id}, { $set: { 
													name: req.body.name, 
													location : req.body.location,  
													description : req.body.description
	}})
	.then(data => res.send(data))
})

router.delete("/item/delete/:id", (req,res) => {
	const id = req.params.id
	Item.findOneAndRemove(id)
	.then(data => res.send("Has been deleted"))
})

module.exports = router